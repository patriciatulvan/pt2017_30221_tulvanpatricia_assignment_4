package entities;

@SuppressWarnings("serial")
public class SavingAccount extends Account  {
	
	private int idAcc; 
	private int idHolder;
	private int balance;
	
	public SavingAccount(int idAcc, int idHolder, int balance){
		super(idAcc,idHolder,balance);
		this.idAcc=idAcc;
		this.idHolder=idHolder;
		this.balance=balance;
	}
	
	public void deposit(Integer sum){
		int init= balance;
		if(sum>= 1000)
			balance+=sum;
		setChanged();
		notifyObservers(init);
		System.out.println("initial="+init+ " now="+balance);
	}

	public void withdraw(Integer sum) {
		int init=balance;
		if(sum>=1000)
			if(balance>=sum){
				balance-=sum;
			}
		setChanged();
		notifyObservers(init);
		System.out.println("initial="+init+ " now="+balance);
	}
	
	public int getHolder(){
		return idHolder;
	}
	
	public int getIdAcc() {
		return idAcc;
	}
	
	public int getSold() {
		return balance;
	}
	
	public void setIdHolder(int idHolder) {
		this.idHolder = idHolder;
	}

}
