package entities;

public interface BankProc {
	
	/**
     *  @pre !isEmpty()
     *  @post getSize()= getSize()@pre - 1
     */
	public void removePerson(Person person);
	
	/**
     *  @pre person != null
     *  @post getSize()= getSize()@pre + 1
     */
	public void addPerson(Person person);
	
	/**
     *  @pre person != null
     *  @post getSize()= getSize()@pre
     */
	public void editPerson(Person person, String name, String phone, String address, String mail);
	
	/**
     *  @pre !isEmpty()
     *  @post @nochange
     */
	public void listPersons();
	
	/**
     *  @pre !isEmpty()
     *  @post getSize()= getSize()@pre - 1
     */
	public void removeAccount(int id, int idHolder);
	
	/**
     *  @pre person != null
     *  @post getSize()= getSize()@pre + 1
     */
	public void addAccount(Person person,Account account);
	
	/**
     *  @post getSize()= getSize()@pre
     */
	public void editAccount(int idH,int id,int idNewH);
	
	/**
     *  @pre !isEmpty()
     *  @post @nochange
     */
	public void listAccounts();
	
	public void readData();
	
	public void writeData();

}
