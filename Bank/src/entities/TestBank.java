package entities;

import static org.junit.Assert.*;
import org.junit.Test ;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class TestBank{
	
	HashMap<Person,Set<Account>> hm;
	 Person mary;
	 Person ana;
	 Set<Account> s;
	 Set<Account> s1;
	
	public void setUp(){
		hm = new HashMap<Person, Set<Account>>();
	
	    mary= new Person(198,"Maria Pop","2231555","0756512898","Str. Memo","maria_pop@yahoo.com");
	    s= new HashSet<Account>();
		s.add(new SpendingAccount(10,198,2000));
		s.add(new SavingAccount(16,198,7000));
		
		ana= new Person(100,"Ana Popa","229871","0750676544","Str. Eminescu","ana.popa@gmail.com");
		s1= new HashSet<Account>();
		s1.add(new SpendingAccount(1,100,5000));
		s1.add(new SavingAccount(2,100,5000));
		
		hm.put(mary,s);
	}
	
	@Test
	public void removePerson(){
		setUp();
		hm.remove(mary);
		assertEquals(0,hm.size());
	}
	
	@Test
	public void addPerson(){
		setUp();
		hm.put(ana, s1);
		assertEquals(2,hm.size());
	}
	
	@Test
	public void editPerson(){
		setUp();
		Person john= new Person(mary.getId(),"John",mary.getCnp(),"0754324341","Str. Eminescu","john21@gmail.com");
		
		hm.put(john,hm.get(mary));
		hm.remove(mary);
		
		assertEquals(1,hm.size());
	}
	
	@Test
	public void removeAccount(){
		setUp();
		hm.put(ana, s1);
		Account found =null;
		for(Person p: hm.keySet()){
		    	for(Set<Account> s: hm.values()){
		    		for(Account a: s){
		    			if(a.getIdAcc()== 10 && 198==p.getId())
		    				found=a;
		    		}
		    		s.remove(found);
		    	}
		}
		assertEquals(1,s.size());
	}
	
	@Test
	public void addAccount(){
		setUp();
		Account a= new SpendingAccount(17,198,500);
		
		hm.remove(mary);
		s.add(a);
		hm.put(mary,s);
		
		assertEquals(3,s.size());
	}
	
	@Test
	public void editAccount(){
		setUp();
		
		Iterator<Account> i= s.iterator();
		Account a= i.next();
		
		s.remove(a);
		s1.add(a);
		
		hm.put(ana,s1);
		
		assertEquals(1,s.size());
		assertEquals(3,s1.size());
	}
	
}
