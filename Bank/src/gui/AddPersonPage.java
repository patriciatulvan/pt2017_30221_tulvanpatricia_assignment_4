package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entities.Bank;
import entities.Person;

public class AddPersonPage extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public AddPersonPage(){
		setTitle("Add Person");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 600, 300);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn = new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelLbl = new JPanel();
		panelLbl.setBackground(Color.LIGHT_GRAY);
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		
		JPanel panelText = new JPanel();
		panelText.setBackground(Color.LIGHT_GRAY);
		panelText.setLayout(new BoxLayout(panelText,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg = new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		panelImg.setLayout(new BoxLayout(panelImg,BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		
		JLabel img;
		ImageIcon ad= new ImageIcon("add.png");
		img= new JLabel(ad);
		panelImg.add(img);
		
		JLabel idLbl = new JLabel();
		idLbl.setText("ID: ");
		idLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel nameLbl= new JLabel();
		nameLbl.setText("Name: ");
		nameLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel cnpLbl = new JLabel();
		cnpLbl.setText("CNP: ");
		cnpLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		
		JLabel phoneLbl = new JLabel();
		phoneLbl.setText("Phone number: ");
		phoneLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel addressLbl = new JLabel();
		addressLbl.setText("Address: ");
		addressLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel mailLbl = new JLabel();
		mailLbl.setText("Mail: ");
		mailLbl.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		panelLbl.add(idLbl);
		panelLbl.add(nameLbl);
		panelLbl.add(cnpLbl);
		panelLbl.add(phoneLbl);
		panelLbl.add(addressLbl);
		panelLbl.add(mailLbl);
		
		JTextField id = new JTextField();
		id.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		id.setColumns(30);
		
		JTextField name= new JTextField();
		name.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		name.setColumns(30);
		
		JTextField cnp = new JTextField();
		cnp.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		cnp.setColumns(30);
		
		JTextField address= new JTextField();
		address.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		address.setColumns(30);
		
		JTextField phone= new JTextField();
		phone.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		phone.setColumns(30);
		
		JTextField mail = new JTextField();
		mail.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		mail.setColumns(30);
		
		panelText.add(id);
		panelText.add(name);
		panelText.add(cnp);
		panelText.add(address);
		panelText.add(phone);
		panelText.add(mail);
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new PersonOpPage();
				setVisible(false);
				dispose();
			}
		});
		
		JButton add = new JButton();
		add.setText("Add Person");
		add.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!id.getText().equals("") && !name.getText().equals("") && !cnp.getText().equals("") && !phone.getText().equals("") && !address.getText().equals("") && !mail.getText().equals("")){
					Bank bank= FirstPage.bank;
					int idp;
					idp= Integer.parseInt(id.getText());
					Person person = new Person(idp,name.getText(),cnp.getText(),phone.getText(),address.getText(),mail.getText());
					bank.addPerson(person);
					JOptionPane.showMessageDialog(null, "Person Added! ", "Add Person", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Add Person", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		JButton reset = new JButton();
		reset.setText("Reset");
		reset.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				id.setText("");
				name.setText("");
				cnp.setText("");
				address.setText("");
				phone.setText("");
				mail.setText("");
			}
		});
		panelBtn.add(back);
		panelBtn.add(reset);
		panelBtn.add(add);
		
		bigPanel.add(panelLbl);
		bigPanel.add(panelText);
		bigPanel.add(panelBtn);
		
		panel.add(panelImg);
		panel.add(bigPanel);
		
		setContentPane(panel);
		setVisible(true);
	}

}
