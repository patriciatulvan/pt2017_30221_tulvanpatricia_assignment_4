package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import entities.Account;
import entities.Bank;

public class DepositPage extends JFrame{
	private static final long serialVersionUID = 1L;
	private int idH;
	private JComboBox<Integer> idA;
	
	public DepositPage(){
		setTitle("Deposit");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 500, 300);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg= new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelLbl= new JPanel();
		panelLbl.setBackground(Color.LIGHT_GRAY);
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		JPanel panelA= new JPanel();
		panelA.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelD= new JPanel();
		panelD.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn= new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon del= new ImageIcon("deposit.png");
		img= new JLabel(del);
		panelImg.add(img);
		
		JLabel giveId = new JLabel();
		giveId.setText("Choose the account's ID: ");
		giveId.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel giveHolder= new JLabel();
		giveHolder.setText("Choose the person's ID");
		giveHolder.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel giveSum= new JLabel();
		giveSum.setText("Give the amount of money: ");
		giveSum.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		panelLbl.add(giveHolder);
		panelLbl.add(giveSum);
		
		
		Bank bank= FirstPage.bank;
		Integer[] idHolders= new Integer[bank.getPersons().size()];
		for(int i=0;i<bank.getPersons().size();i++)
			idHolders[i]=bank.getPersons().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(idHolders);
		idCombo.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		panelD.add(idCombo);
		
		JTextField sum = new JTextField();
		sum.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		sum.setColumns(10);
		sum.setText("");
			
		panel.add(idCombo);
		panel.add(sum);
		
		panelA.add(panelLbl);
		panelA.add(panel);
		
		JButton back= new JButton();
		back.setText("Back");
		back.setBackground(new Color(153,255,153));
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		panelBtn.add(back);
		
		
		JButton setH= new JButton();
		setH.setText("Set holder");
		setH.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		setH.setBackground(new Color(153,255,153));
		setH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0){
					Integer idHolder=(Integer)idCombo.getSelectedItem();
					System.out.println(idHolder);
					Integer[] id = new Integer[bank.getAccounts(idHolder).size()];
					for(int i=0;i<bank.getAccounts(idHolder).size();i++)
						id[i]=bank.getAccounts(idHolder).get(i).getIdAcc();
					JComboBox<Integer> idAcc= new JComboBox<>(id);
					idAcc.setFont(new Font("Book Antiqua", Font.BOLD, 13));
				
					panelD.removeAll();
					panelD.add(giveId);
					panelD.add(idAcc);
				
					saveHolder(idHolder);
					saveCombo(idAcc);
				
					setContentPane(bigPanel);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Deposit", JOptionPane.ERROR_MESSAGE);
			}
		});
		panelBtn.add(setH);
		
		JButton deposit = new JButton();
		deposit.setText("Deposit money");
		deposit.setBackground(new Color(153,255,153));
		deposit.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		deposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0){
					int idAcc= (Integer)idA.getSelectedItem();
					ArrayList<Account> accounts= bank.getAccounts(idH);
					Account account= null;
					for(Account a: accounts)
						if(a.getIdAcc()==idAcc)
							account=a;
					int init = account.getSold();
					if(!sum.getText().equals("")){
						account.deposit(Integer.parseInt(sum.getText()));
						bank.writeData();
						
						if(init< account.getSold()){
							JOptionPane.showMessageDialog(null, "Successful Operation! \n Initial balance="+init+" Current balance="+account.getSold(), "Deposit", JOptionPane.INFORMATION_MESSAGE);
						}
						else 
							JOptionPane.showMessageDialog(null, "Failed Operation! \nThis is a Saving Account. You have to deposit at least 1000 lei!", "Deposit", JOptionPane.ERROR_MESSAGE);
					}
					else
						JOptionPane.showMessageDialog(null, "Failed Operation! ", "Deposit", JOptionPane.ERROR_MESSAGE);
					}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Deposit", JOptionPane.ERROR_MESSAGE);
				}
		});
		panelBtn.add(deposit);
		
		bigPanel.add(panelImg);
		bigPanel.add(panelA);
		bigPanel.add(panelD);
		bigPanel.add(panelBtn);
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	private void saveHolder(int idHolder){
		idH=idHolder;
	}
	
	private void saveCombo(JComboBox<Integer> combo){
		idA=combo;
	}
}
