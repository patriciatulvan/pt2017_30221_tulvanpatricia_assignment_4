package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import entities.Bank;
import entities.Person;

public class RemovePersonPage extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public RemovePersonPage(){
		setTitle("Remove Person");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 350, 250);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		bigPanel.setLayout(new BoxLayout(bigPanel,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg= new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelText= new JPanel();
		panelText.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn= new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon del= new ImageIcon("delete.png");
		img= new JLabel(del);
		panelImg.add(img);
		
		JLabel giveId = new JLabel();
		giveId.setText("Choose the person's ID: ");
		giveId.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		
		Bank bank= FirstPage.bank;
		Integer[] ids= new Integer[bank.getPersons().size()];
		for(int i=0;i<bank.getPersons().size();i++)
			ids[i]=bank.getPersons().get(i).getId();
		JComboBox<Integer> idCombo= new JComboBox<>(ids);
		idCombo.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		panelText.add(idCombo);
		
		panelText.add(giveId);
		panelText.add(idCombo);
		
		JButton back= new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new PersonOpPage();
				setVisible(false);
				dispose();
			}
		});
		panelBtn.add(back);
		
		JButton delete = new JButton();
		delete.setText("Remove Person");
		delete.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0){
					Person person= bank.getPerson((Integer)idCombo.getSelectedItem());
					bank.removePerson(person);
					JOptionPane.showMessageDialog(null, "Successful Operation! ", "Remove Person", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Remove Person", JOptionPane.ERROR_MESSAGE);
			}
		});
		panelBtn.add(delete);
		
		bigPanel.add(panelImg);
		bigPanel.add(panelText);
		bigPanel.add(panelBtn);
		setContentPane(bigPanel);
		setVisible(true);
	}

}
