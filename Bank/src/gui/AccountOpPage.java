package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AccountOpPage extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public AccountOpPage(){
		setTitle("Account Operations");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 620,200);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel accountPanel = new JPanel();
		accountPanel.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon folder= new ImageIcon("folder.png");
		img= new JLabel(folder);
		bigPanel.add(img);
		
		JButton addC = new JButton();
		addC.setText("Add Account");
		addC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		addC.setBackground(new Color(255,204,229));
		addC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AddAccountPage();
				setVisible(false);
				dispose();
			}
		});
		accountPanel.add(addC);
		
		JButton deleteC = new JButton();
		deleteC.setText("Remove Account");
		deleteC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		deleteC.setBackground(new Color(255,204,229));
		deleteC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new RemoveAccountPage();
				setVisible(false);
				dispose();
			}
		});
		accountPanel.add(deleteC);
		
		JButton editC = new JButton();
		editC.setText("Edit Account");
		editC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		editC.setBackground(new Color(255,204,229));
		editC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new EditAccountPage();
				setVisible(false);
				dispose();
			}
		});
		accountPanel.add(editC);
		
		JButton viewC = new JButton();
		viewC.setText("List Accounts");
		viewC.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		viewC.setBackground(new Color(255,204,229));
		viewC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ListAccountsPage();
				setVisible(false);
				dispose();
			}
		});
		accountPanel.add(viewC);
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.setBackground(new Color(153,204,255));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});
		accountPanel.add(back);
		
		bigPanel.add(accountPanel);
		setContentPane(bigPanel);
		setVisible(true);
	}
}
