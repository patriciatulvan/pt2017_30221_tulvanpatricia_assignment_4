package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import entities.Bank;

public class FirstPage extends JFrame{
	private static final long serialVersionUID = 1L;
	public static Bank bank;
	
	public FirstPage(){
		bank=new Bank();
		setTitle("Bank Application");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 400, 270);
		init();
	}
	
	public void init(){
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon bank= new ImageIcon("bank.png");
		img= new JLabel(bank);
		panel.add(img);
		
		JButton personOp = new JButton();
		personOp.setText("Person operations");
		personOp.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		personOp.setBackground(new Color(51,153,255));
		personOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new PersonOpPage();
			}
		});
		
		JButton accountOp= new JButton();
		accountOp.setText("Account operations");
		accountOp.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		accountOp.setBackground(new Color(51,153,255));
		accountOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountOpPage();
			}
		});
		
		JButton deposit = new JButton();
		deposit.setText("Deposit");
		deposit.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		deposit.setBackground(new Color(255,255,102));
		deposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new DepositPage();
			}
		});
		
		JButton withdraw = new JButton();
		withdraw.setText("Withdraw");
		withdraw.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		withdraw.setBackground(new Color(255,255,102));
		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new WithdrawPage();
			}
		});
		
		JPanel opPanel = new JPanel();
		opPanel.setBackground(Color.LIGHT_GRAY);
		
		opPanel.add(personOp);
		opPanel.add(accountOp);
		
		JPanel monOp= new JPanel();
		monOp.setBackground(Color.LIGHT_GRAY);
		monOp.add(deposit);
		monOp.add(withdraw);
		
		JPanel bigPanel= new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		bigPanel.add(panel);
		bigPanel.add(opPanel);
		bigPanel.add(monOp);
		setContentPane(bigPanel);
		setVisible(true);
	}
	
	public static void main(String[] args){
		new FirstPage();
		
	}

}
