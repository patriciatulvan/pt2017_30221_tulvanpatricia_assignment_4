package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.Bank;

public class EditAccountPage extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public EditAccountPage(){
		setTitle("Edit Account");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 400, 250);
		init();
	}
	
	public void init(){
		JPanel bigPanel = new JPanel();
		bigPanel.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelBtn = new JPanel();
		panelBtn.setBackground(Color.LIGHT_GRAY);
		
		JPanel panelLbl = new JPanel();
		panelLbl.setBackground(Color.LIGHT_GRAY);
		panelLbl.setLayout(new BoxLayout(panelLbl,BoxLayout.PAGE_AXIS));
		
		JPanel panelText = new JPanel();
		panelText.setBackground(Color.LIGHT_GRAY);
		panelText.setLayout(new BoxLayout(panelText,BoxLayout.PAGE_AXIS));
		
		JPanel panelImg = new JPanel();
		panelImg.setBackground(Color.LIGHT_GRAY);
		panelImg.setLayout(new BoxLayout(panelImg,BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		
		JLabel img;
		ImageIcon e= new ImageIcon("edit.png");
		img= new JLabel(e);
		panelImg.add(img);
		
		JLabel idHolder = new JLabel();
		idHolder.setText("Choose the person's ID: ");
		idHolder.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel idNewHolder = new JLabel();
		idNewHolder.setText("Choose the new holder's ID: ");
		idNewHolder.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		
		JLabel idAccount = new JLabel();
		idAccount.setText("Choose the account's ID: ");
		idAccount.setFont(new Font("Book Antiqua", Font.BOLD, 14));
	
		panelLbl.add(idAccount);
		panelLbl.add(idHolder);
		panelLbl.add(idNewHolder);
		
	
		Bank bank= FirstPage.bank;
		Integer[] idHolders= new Integer[bank.getPersons().size()];
		for(int i=0;i<bank.getPersons().size();i++)
			idHolders[i]=bank.getPersons().get(i).getId();
		JComboBox<Integer> idHolderC= new JComboBox<>(idHolders);
		idHolderC.setFont(new Font("Book Antiqua", Font.BOLD, 12));
		
		JComboBox<Integer> idHolderNC= new JComboBox<>(idHolders);
		idHolderNC.setFont(new Font("Book Antiqua", Font.BOLD, 12));
		
		Integer[] idAccounts= new Integer[bank.getAllAccounts().size()];
		for(int i=0;i<bank.getAllAccounts().size();i++)
			idAccounts[i]=bank.getAllAccounts().get(i).getIdAcc();
		JComboBox<Integer> idAccountsC= new JComboBox<>(idAccounts);
		idAccountsC.setFont(new Font("Book Antiqua", Font.BOLD, 12));
		
		panelText.add(idAccountsC);
		panelText.add(idHolderC);
		panelText.add(idHolderNC);
				
		
		JButton back = new JButton();
		back.setText("Back");
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountOpPage();
				setVisible(false);
				dispose();
			}
		});
		
		JButton edit = new JButton();
		edit.setText("Edit Account");
		edit.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(bank.getPersons().size()>0 && bank.getAllAccounts().size()>0){
					int idH = (Integer)idHolderC.getSelectedItem();
					int idNH= (Integer)idHolderNC.getSelectedItem();
					int idA= (Integer)idAccountsC.getSelectedItem();
					System.out.println("iDH="+idH+" idNH="+idNH+" idA="+idA);
					bank.editAccount(idH, idA, idNH);
					JOptionPane.showMessageDialog(null, "Successful Operation! ", "Edit Account", JOptionPane.INFORMATION_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "Edit Account", JOptionPane.ERROR_MESSAGE);
			}
		});
	
		panelBtn.add(back);
		panelBtn.add(edit);
		
		bigPanel.add(panelLbl);
		bigPanel.add(panelText);
		bigPanel.add(panelBtn);
		panel.add(panelImg);
		panel.add(bigPanel);
		
		setContentPane(panel);
		setVisible(true);
	}
}
