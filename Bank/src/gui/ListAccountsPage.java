package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import entities.Bank;

public class ListAccountsPage extends JFrame{
	private static final long serialVersionUID = 1L; 

	public ListAccountsPage(){
		setTitle("Accounts Table");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 100, 600, 550);
		init();
	}
	
	public void init(){
		JPanel panel= new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		
		JLabel img;
		ImageIcon list= new ImageIcon("list.png");
		img= new JLabel(list);
		panel.add(img);
		
		JButton back = new JButton();
		back.setText("Back");
		back.setBackground(new Color(204,153,255));
		back.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AccountOpPage();
				setVisible(false);
				dispose();
			}
		});
		
		JButton print = new JButton();
		print.setText("Show table");
		print.setBackground(new Color(204,153,255));
		print.setFont(new Font("Book Antiqua", Font.BOLD, 14));
		print.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Bank bank= FirstPage.bank; 
				if(bank.getAllAccounts().size()>0){
					ArrayList<Object> list= new ArrayList<Object>();
					list.addAll(bank.getAllAccounts());
				
					JTable table= bank.createTable(list);
					table.setFont(new Font("Book Antiqua", Font.BOLD, 14));
				
					JScrollPane scroll = new JScrollPane(table);
					panel.add(table.getTableHeader());
					panel.add(scroll);
				
					setContentPane(panel);
				}
				else
					JOptionPane.showMessageDialog(null, "Failed Operation! No data available! ", "List Accounts", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		
		panel.add(back);
		panel.add(print);
		
		setContentPane(panel);
		setVisible(true);
	}
}
